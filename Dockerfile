FROM debian:stable-slim

WORKDIR /seafile

# Install packages
RUN apt update -y
RUN apt install build-essential -y
RUN apt install libevent-dev libcurl4-openssl-dev libglib2.0-dev \
        uuid-dev intltool libsqlite3-dev \
        libmariadb-dev-compat libmariadb-dev \
        libarchive-dev libtool libjansson-dev valac libfuse-dev \
        re2c flex python-setuptools cmake -y
#         libmysqlclient-dev \

# Compile development libraries
## libevhtp is a http server libary on top of libevent. It's used in seafile file server.
## After compiling all the libraries, run ldconfig to update the system libraries cache.
RUN git clone https://www.github.com/haiwen/libevhtp.git && \
    cd libevhtp && \
    cmake -DEVHTP_DISABLE_SSL=ON -DEVHTP_BUILD_SHARED=OFF . && \
    make && \
    make install && \
    ldconfig

# Install python libraries
RUN mkdir -p /opt/seahub_thirdpart && mkdir -p /tmp
ADD https://pypi.python.org/packages/source/p/pytz/pytz-2016.1.tar.gz /tmp/
ADD https://www.djangoproject.com/m/releases/1.8/Django-1.8.18.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/d/django-statici18n/django-statici18n-1.1.3.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/d/djangorestframework/djangorestframework-3.3.2.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/d/django_compressor/django_compressor-1.4.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/j/jsonfield/jsonfield-1.0.3.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/d/django-post_office/django-post_office-2.0.6.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/g/gunicorn/gunicorn-19.4.5.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/f/flup/flup-1.0.2.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/c/chardet/chardet-2.3.0.tar.gz /tmp/
ADD https://labix.org/download/python-dateutil/python-dateutil-1.5.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/s/six/six-1.9.0.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/d/django-picklefield/django-picklefield-0.3.2.tar.gz /tmp/
ADD https://github.com/haiwen/django-constance/archive/bde7f7c.zip /tmp/
ADD https://pypi.python.org/packages/source/j/jdcal/jdcal-1.2.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/e/et_xmlfile/et_xmlfile-1.0.1.tar.gz /tmp/
ADD https://pypi.python.org/packages/source/o/openpyxl/openpyxl-2.3.0.tar.gz /tmp/
ADD https://pypi.python.org/packages/cc/26/b61e3a4eb50653e8a7339d84eeaa46d1e93b92951978873c220ae64d0733/futures-3.1.1.tar.gz /tmp/
ADD https://pypi.python.org/packages/a8/07/947dfe63dff1f2be5f84eb7f0ff5f712bb1dc730a6499b0aa0be5c8f194e/django-formtools-2.0.tar.gz /tmp/
ADD https://pypi.python.org/packages/87/16/99038537dc58c87b136779c0e06d46887ff5104eb8c64989aac1ec8cba81/qrcode-5.3.tar.gz /tmp/
RUN cd /opt/seahub_thirdpart && \
    export PYTHONPATH=. && \
    pip install -t /opt/seahub_thirdpart/ /tmp/pytz-2016.1.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/Django-1.8.10.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/django-statici18n-1.1.3.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/djangorestframework-3.3.2.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/django_compressor-1.4.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/jsonfield-1.0.3.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/django-post_office-2.0.6.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/gunicorn-19.4.5.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/flup-1.0.2.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/chardet-2.3.0.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/python-dateutil-1.5.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/six-1.9.0.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/django-picklefield-0.3.2.tar.gz && \
    wget -O /tmp/django_constance.zip https://github.com/haiwen/django-constance/archive/bde7f7c.zip && \
    pip install -t /opt/seahub_thirdpart/ /tmp/django_constance.zip && \
    pip install -t /opt/seahub_thirdpart/ /tmp/jdcal-1.2.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/et_xmlfile-1.0.1.tar.gz && \
    pip install -t /opt/seahub_thirdpart/ /tmp/openpyxl-2.3.0.tar.gz && \
    rm -rf /tmp/*.tar.gz

RUN export PKG_CONFIG_PATH=/seafile/seafile/lib:/seafile/libsearpc:/seafile/ccnet:$PKG_CONFIG_PATH
